var pageCount = 0; // Works count on onepager page
var pageSize = 20;
var activePageIndex = 0; // What page to display
function searchAcrossAllFilters(pageIndex = 0) {
 let filteredWorks = allWorks;
 filteredWorks = searchType(filteredWorks);
 filteredWorks = searchYear(filteredWorks);
 filteredWorks = setUpPager(filteredWorks, pageIndex);
 displayList(filteredWorks);
}
function setUpPager(works, pageIndex) {
 pageCount = Math.ceil(works.length / pageSize);
 let startWorkIndex = pageIndex * pageSize;
 let endWorkIndex = startWorkIndex + pageSize - 1;
 let pageWorks = [];
 for (let i = startWorkIndex; i <= endWorkIndex && i < works.length; i++) {
   pageWorks.push(works[i]);
 }
 displayPager();
 return pageWorks;
}
function displayPager() {
 let pagerHtml = "";
 pagerHtml += `<a class="page-link" href="#Prev" id="prevPage" title="Previous">
   <span aria-hidden="true">&laquo;</span>
   <span class="sr-only">Previous</span>
 </a>`;
 for (let i = 0; i < pageCount; i++) {
   pagerHtml += `<a class="page-link" href="javascript:searchAcrossAllFilters(${i})">${i+1}</a>`;
 }
 pagerHtml += `<a class="page-link" href="#Next" id="nextPage" title="Next">
   <span aria-hidden="true">&raquo;</span>
   <span class="sr-only">Next</span>
 </a>`;
 document.getElementById("myWorksPagerDiv").innerHTML = pagerHtml;
}



function searchType(currentWorks) {
    let workType = document.getElementById("myWorkType").value;
    let filteredWorks = [];
    for (i = 0; i < currentWorks.length; i++){
      if (currentWorks[i].workType.toLowerCase().search(workType.toLowerCase()) >= 0){ // kui ei leia midagi, siis on tulemus -1
        filteredWorks.push(currentWorks[i]);
        } 
    } 
    return filteredWorks;
} 


function searchYear(currentWorks) {
  let workYear = document.getElementById("myYear").value;
  let filteredYears = [];
  for (i = 0; i < currentWorks.length; i++){                  // allWorks on kogu tabeli massiiv
    if (workYear == null || workYear == "" || parseInt(currentWorks[i].year) == parseInt(workYear)){
      filteredYears.push(currentWorks[i]);
    } 
  } 
  return filteredYears;
} 
