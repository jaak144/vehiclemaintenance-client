function generateTopMenu() {
    let logoutLink = "";
    if (!isEmpty(getUsername())) {
        logoutLink = `${getUsername()} | <a href="javascript:logoutUser()" style="color: #fff; font-weight: bold;">logi välja</a>`;
    }

    document.getElementById("topMenuContainer").innerHTML = `
        <div style="padding: 5px; color: white; font-style: italic; background:black;">
            <div class="row">
                <div class="col-sm-4">
                    <strong>Vali IT!</strong>
                </div>
                <div class="col-sm-4" style="text-align: center; font-style: normal;">
                <a href="chart.html">Statistika</a> 
                </div>
                <div class="col-sm-4" style="text-align: right; font-style: normal;">
                    ${logoutLink}
                </div>
            </div>
        </div>
    `;
}

function showLoginContainer() {
    document.getElementById("loginContainer").style.display = "block";
    document.getElementById("mainContainer").style.display = "none";
}

function showMainContainer() {
    document.getElementById("loginContainer").style.display = "none";
    document.getElementById("mainContainer").style.display = "block";
}

function changeView() {
    if(document.getElementById("viewSelect").value === "CHART") {
        showCompanyChart();
    } else {
        showCompanyList();
    }
}

function showCompanyList() {
    document.getElementById("companyList").style.display = "block";
    document.getElementById("companyChart").style.display = "none";
    loadCompanies();
}

function showWorkChart() {
    document.getElementById("workList").style.display = "none";
    document.getElementById("workChart").style.display = "block";
    loadChart();
}
