// Controller functions here...
var allWorks = [];
function loadWork() {
   fetchWork().then(
       function(sisendinfo){
           allWorks = sisendinfo;
           searchAcrossAllFilters();
       }
   );
}

function displayList(sisendinfo) {
    let workHtml = "";

    for(let i = 0; i < sisendinfo.length; i++) {
        workHtml = workHtml + `
            <tr>
                <td>
                    ${sisendinfo[i].id}
                </td>
                <td>
                    ${sisendinfo[i].year}
                </td>
                <td>
                    ${sisendinfo[i].km}
                </td>
                <td>
                    ${sisendinfo[i].work}
                </td>
                <td>
                    ${sisendinfo[i].producer}
                </td>
                <td>
                    ${sisendinfo[i].price}
                </td>
                <td>
                    ${sisendinfo[i].workType}
                </td>
                <td>
                    ${sisendinfo[i].notes}
                </td>
                <td>
                <div class="btn-group">
                    <button class="btn btn-danger" 
                        onclick="handleDeleteButtonClick(${sisendinfo[i].id})">
                        Kustuta
                    </button>
                    <br>
                    <button class="btn btn-info"
                        onclick="handleEditButtonClick(${sisendinfo[i].id})">
                        Muuda
                    </button>
                </div>
                </td>
            </tr>
        `;
    }

    document.getElementById("workList").innerHTML = workHtml;
}

function handleDeleteButtonClick(id) {
    if (confirm("Oled sa ikka kindel, et soovid seda raske tööga tulnud kannet kustutada?")) {
        deleteWork(id).then(loadWork);
    }
}
function searchWork(id) {
    for (i = 0; i < allWorks.length; i++) {
        if (allWorks[i].id == id) {
            return allWorks[i];
        }
    }
}

function handleEditButtonClick(id) { // allWorks asukohal id, kas on olemas ja too kõik järgnev info välja
let work = searchWork(id);
                    document.getElementById("id").value = work.id;
                    document.getElementById("Year").value = work.year;
                    document.getElementById("KM").value = work.km;
                    document.getElementById("Work").value = work.work;
                    document.getElementById("Producer").value = work.producer;
                    document.getElementById("Price").value = work.price;
                    document.getElementById("WorkType").value = work.workType;
                    document.getElementById("Notes").value = work.notes;
                    $("#workModal").modal("show");

                }
    

function handleSave() {
    if (isFormValid() === false) {
        return;
    }
    if (document.getElementById("id").value > 0) {
        // Edit
        handleEdit();
    } else {
        // Add
        handleAdd();
    }
}

function handleEdit() {
    let work = {
        id: document.getElementById("id").value,
        year: document.getElementById("Year").value,
        km: document.getElementById("KM").value,
        work: document.getElementById("Work").value,
        producer: document.getElementById("Producer").value,
        price: document.getElementById("Price").value,
        workType: document.getElementById("WorkType").value,
        notes: document.getElementById("Notes").value,
    };

    putWork(work).then(
        function() {
            loadWork();
            $("#workModal").modal("hide");
        }
    );
}

function handleAddButtonClick() {
    $("#workModal").modal("show");
    document.getElementById("id").value = null;
    document.getElementById("Year").value = null;
    document.getElementById("KM").value = null;
    document.getElementById("Work").value = null;
    document.getElementById("Producer").value = null;
    document.getElementById("Price").value = null;
    document.getElementById("WorkType").value = null;
    document.getElementById("Notes").value = null;
}

function handleAdd() {
    let work = {
        year: document.getElementById("Year").value,
        km: document.getElementById("KM").value,
        work: document.getElementById("Work").value,
        producer: document.getElementById("Producer").value,
        price: document.getElementById("Price").value,
        workType: document.getElementById("WorkType").value,
        notes: document.getElementById("Notes").value,
    };

    postWork(work).then(
        function() {
            loadWork();
            $("#workModal").modal("hide");
        }
    );
}

// Validation
function isFormValid() {
    let year = document.getElementById("Year").value;
    let km = document.getElementById("KM").value;
    let work = document.getElementById("Work").value;
    let producer = document.getElementById("Producer").value;
    let price = document.getElementById("Price").value;
    let workType = document.getElementById("WorkType").value;
    let notes = document.getElementById("Notes").value;



    if (year === null || year.length < 1) {
        document.getElementById("errorMessage").innerText = "Aasta on kohustuslik!";
        document.getElementById("errorMessage").style.display = "block";
        return false;
    }
    if (km === null || km.length < 1) {
        document.getElementById("errorMessage").innerText = "Kilomeeter on kohustuslik!";
        document.getElementById("errorMessage").style.display = "block";
        return false;
    }
    if (work === null || work.length < 1) {
        document.getElementById("errorMessage").innerText = "Töö on kohustuslik!";
        document.getElementById("errorMessage").style.display = "block";
        return false;
    }
    if (producer === null || producer.length < 1) {
    document.getElementById("errorMessage").innerText = "Tarnija on kohustuslik!";
    document.getElementById("errorMessage").style.display = "block";
    return false;
    }
    if (price === null || price.length < 1) {
        document.getElementById("errorMessage").innerText = "Kulu on kohustuslik!";
        document.getElementById("errorMessage").style.display = "block";
        return false;
        }
        if (workType === null || workType.length < 1) {
            document.getElementById("errorMessage").innerText = "Tüüp on kohustuslik!";
            document.getElementById("errorMessage").style.display = "block";
            return false;
            }
            if (notes === null || notes.length < 1) {
                document.getElementById("errorMessage").innerText = "Märkus on kohustuslik!";
                document.getElementById("errorMessage").style.display = "block";
                return false;
                }

    document.getElementById("errorMessage").style.display = "none";
    return true;
}

function loginUser() {
    let credentials = getCredentialsFromLoginContainer();
    if (validateCredentials(credentials)) {
        login(credentials).then(session => {
            storeAuthentication(session);
            generateTopMenu();
            loadWork();
        })
    }
}

function logoutUser() {
    clearAuthentication();
    generateTopMenu();
    showLoginContainer();
}

function closeWorkModal (){
    $("#workModal").modal("hide");
}
