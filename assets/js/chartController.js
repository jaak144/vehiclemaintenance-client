
var allWorks = [];

function loadWork() {
    fetchWork().then(
        function(sisendinfo){
            allWorks = sisendinfo;
            displayChart();
        }
    );
} 

let types = ["Hooldus", "Remont", "Uuendus", "Näit"];

function displayChart(){
    document.getElementById("workChart").innerHTML = `
        <div class="row justify-content-center">
            <div class="col-lg-10" style="text-align: center; color: white">
                <strong style="font-size: 28px">Kulud tüüpide kaupa</strong>
            </div>
        </div>
        <br>
        <br>
        <div class="row justify-content-center">
            <div class="col-lg-10">
                <canvas id="workChartCanvas"></canvas>
            </div>
        </div>
    `;

    let ctx = document.getElementById('workChartCanvas').getContext('2d');
    let chartData = composeChartData();
    let chart = new Chart(ctx, {
        // The type of chart we want to create
        type: 'doughnut',
        data: chartData,
        // Configuration options go here
        options: {}
    });
}

// Chart Functions

function generateRandomColor() {
    let r = Math.floor(Math.random() * 255);
    let g = Math.floor(Math.random() * 255);
    let b = Math.floor(Math.random() * 255);
    return `rgb(${r},${g},${b})`;
}

function composeChartDataset() {
    if (allWorks != null && allWorks.length > 0) {
        let pricesByType = [];
        for (let i=0; i < types.length; i++) {
            let filteredWorks = allWorks.filter(work => (work.workType === types[i]));
            let sum = 0;
            for (let j = 0; j < filteredWorks.length; j++) {
                sum = sum + filteredWorks[j].price;
            }
            pricesByType.push(sum);
        }
        let backgroundColors = types.map(generateRandomColor);
        return [{
            data: pricesByType,
            backgroundColor: backgroundColors,
        }];
    }
    return [];
}

function composeChartData() {
    return {
        datasets: composeChartDataset(),
        labels: types
    };
}