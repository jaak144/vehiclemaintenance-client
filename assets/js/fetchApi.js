
// Functions for communicating with backend-API here...

function fetchWork() {
    return fetch(
        `${API_URL}/sisendinfo`,
        {
            method: 'GET',
            headers: {
                'Authorization': `Bearer ${getToken()}`
            }
        }
    )
    .then(checkResponse)
    .then(sisendinfo => sisendinfo.json());
}

function deleteWork(id) {
    return fetch(`${API_URL}/sisendinfo?id=${id}`, {
        method: 'DELETE',
        headers: {
            'Authorization': `Bearer ${getToken()}`
        }
    }
)
.then(checkResponse);
}

function login(credentials) {
return fetch(
    `${API_URL}/users/login`,
    {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(credentials)
    }
)
.then(checkResponse)
.then(session => session.json());
}

function checkResponse(response) {
if (!response.ok) {
    clearAuthentication();
    showLoginContainer();
    closeWorkModal();
    generateTopMenu();
    throw new Error(response.status);
}
showMainContainer();
return response;
}


// Work changing
function putWork(sisendinfo) {
    return fetch(
        `${API_URL}/sisendinfo`, 
        {
            method: 'PUT', 
            headers: {
                "Content-Type": "application/json",
                'Authorization': `Bearer ${getToken()}`
            },
            body: JSON.stringify(sisendinfo)
        }
    );
}

// Work adding
function postWork(sisendinfo) {
    return fetch(
        `${API_URL}/sisendinfo`, 
        {
            method: 'POST', 
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${getToken()}`
            },
            body: JSON.stringify(sisendinfo)
        }
    )
    .then(checkResponse);

}

